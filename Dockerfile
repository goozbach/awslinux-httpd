FROM amazonlinux:latest

RUN yum install -y iproute sysstat procps-ng httpd; yum clean all

EXPOSE 80/tcp 443/tcp

CMD ["/usr/sbin/httpd", "-DFOREGROUND"]

